/**
 * general plugin functions
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module npp_api.pluginfunc;


public import core.sys.windows.windows;
public import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
public import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
public import npp_api.PowerEditor.menuCmdID;
public import npp_api.PowerEditor.resource;
public import npp_api.pluginfunc.auto_pFunc;
public import npp_api.pluginfunc.basic;
public import npp_api.pluginfunc.basic_interface;
public import npp_api.pluginfunc.config_file;
public import npp_api.pluginfunc.export_info;
public import npp_api.pluginfunc.extra_interfece;
public import npp_api.pluginfunc.ini_setting;
public import npp_api.pluginfunc.json_setting;
public import npp_api.pluginfunc.lang;
public import npp_api.pluginfunc.menu;
public import npp_api.pluginfunc.npp_msgs;
public import npp_api.pluginfunc.path;
public import npp_api.pluginfunc.scintilla_msg;
public import npp_api.pluginfunc.shortcut;
public import npp_api.pluginfunc.string;
public import npp_api.scintilla.Sci_Position;
public import npp_api.scintilla.Scintilla;
