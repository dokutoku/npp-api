/**
 * ショートカットをいろいろする
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module npp_api.pluginfunc.shortcut;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winuser;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private static import npp_api.pluginfunc.menu;

pure nothrow @safe @nogc @live
void set_ACCEL(ref core.sys.windows.winuser.ACCEL HACCEL_shortcut, const ref npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey npp_shortcut, core.sys.windows.windef.WORD cmd = 0)

	do
	{
		HACCEL_shortcut.cmd = cmd;
		HACCEL_shortcut.key = npp_shortcut._key;
		HACCEL_shortcut.fVirt =
			((npp_shortcut._isCtrl) ? (core.sys.windows.winuser.FCONTROL) : (0))
			| ((npp_shortcut._isAlt) ? (core.sys.windows.winuser.FALT) : (0))
			| ((npp_shortcut._isShift) ? (core.sys.windows.winuser.FSHIFT) : (0))
			| (core.sys.windows.winuser.FVIRTKEY);
	}

version (Not_betterC):

pure nothrow @safe @nogc @live
size_t sub_menu_shortcuts_length(const npp_api.pluginfunc.menu.sub_menu_index[] menu_list)

	do
	{
		size_t length;

		for (size_t i = 0; i < menu_list.length; i++) {
			if ((menu_list[i].depth > 1) && (menu_list[i].func_item._pShKey != null)) {
				length++;
			}
		}

		return length;
	}

pure nothrow @safe @nogc @live
core.sys.windows.winuser.ACCEL[shortcut_length] create_sub_menu_shortcuts(size_t shortcut_length)(const ref npp_api.pluginfunc.menu.sub_menu_index[] menu_list)

	in
	{
		assert(shortcut_length == sub_menu_shortcuts_length(menu_list));
	}

	do
	{
		core.sys.windows.winuser.ACCEL[shortcut_length] output;

		size_t length = 0;

		for (size_t i = 0; i < menu_list.length; i++) {
			if ((menu_list[i].depth > 1) && (menu_list[i].func_item._pShKey != null)) {
				.set_ACCEL(output[length], *(menu_list[i].func_item._pShKey));
				length++;
			}
		}

		return output;
	}
