/**
 * scintilla message wrapper
 *
 * See_Also:
 *      https://www.scintilla.org/ScintillaDoc.html
 *
 * ToDo: recheck
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module npp_api.pluginfunc.scintilla_msg;


version (Windows):

private static import core.stdc.stdint;
private static import core.sys.windows.windef;
private static import core.sys.windows.winuser;
private static import npp_api.scintilla.Scintilla;

alias position = core.stdc.stdint.intptr_t;
alias line = core.stdc.stdint.intptr_t;
alias pointer = void*;

//ToDo:
alias colour = void*;

//alias alpha = ;

pragma(inline, true):
nothrow @nogc:

/+
void send_SCI_START(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_START, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_OPTIONAL_START(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_OPTIONAL_START, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LEXER_START(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LEXER_START, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

void send_SCI_ADDTEXT(core.sys.windows.windef.HWND _scintillaHandle, .position length, const char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ADDTEXT, cast(core.sys.windows.windef.WPARAM)(length), cast(core.sys.windows.windef.LPARAM)(text));
	}

/+
void send_SCI_ADDSTYLEDTEXT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ADDSTYLEDTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

void send_SCI_INSERTTEXT(core.sys.windows.windef.HWND _scintillaHandle, .position pos, const char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INSERTTEXT, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(text));
	}

void send_SCI_CHANGEINSERTION(core.sys.windows.windef.HWND _scintillaHandle, .position length, const char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHANGEINSERTION, cast(core.sys.windows.windef.WPARAM)(length), cast(core.sys.windows.windef.LPARAM)(text));
	}

void send_SCI_CLEARALL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CLEARALL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

/+
void send_SCI_DELETERANGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DELETERANGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CLEARDOCUMENTSTYLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CLEARDOCUMENTSTYLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETLENGTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLENGTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETCHARAT(core.sys.windows.windef.HWND _scintillaHandle, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCHARAT, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_GETCURRENTPOS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCURRENTPOS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_GETANCHOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETANCHOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETSTYLEAT(core.sys.windows.windef.HWND _scintillaHandle, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSTYLEAT, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(0)));
	}

void send_SCI_REDO(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_REDO, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

/+
void send_SCI_SETUNDOCOLLECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETUNDOCOLLECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SELECTALL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SELECTALL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETSAVEPOINT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSAVEPOINT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETSTYLEDTEXT(core.sys.windows.windef.HWND _scintillaHandle, ref npp_api.scintilla.Scintilla.Sci_TextRange tr)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSTYLEDTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(&tr)));
	}

bool send_SCI_CANREDO(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CANREDO, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_MARKERLINEFROMHANDLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t markerHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERLINEFROMHANDLE, cast(core.sys.windows.windef.WPARAM)(markerHandle), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_MARKERDELETEHANDLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERDELETEHANDLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETUNDOCOLLECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETUNDOCOLLECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETVIEWWS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETVIEWWS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETVIEWWS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETVIEWWS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETTABDRAWMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTABDRAWMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETTABDRAWMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETTABDRAWMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_POSITIONFROMPOINT(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t x, core.stdc.stdint.intptr_t y)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_POSITIONFROMPOINT, cast(core.sys.windows.windef.WPARAM)(x), cast(core.sys.windows.windef.LPARAM)(y)));
	}

.position send_SCI_POSITIONFROMPOINTCLOSE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t x, core.stdc.stdint.intptr_t y)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_POSITIONFROMPOINTCLOSE, cast(core.sys.windows.windef.WPARAM)(x), cast(core.sys.windows.windef.LPARAM)(y)));
	}

void send_SCI_GOTOLINE(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GOTOLINE, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0));
	}

void send_SCI_GOTOPOS(core.sys.windows.windef.HWND _scintillaHandle, .position caret)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GOTOPOS, cast(core.sys.windows.windef.WPARAM)(caret), cast(core.sys.windows.windef.LPARAM)(0));
	}

/+
void send_SCI_SETANCHOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETANCHOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETCURLINE(core.sys.windows.windef.HWND _scintillaHandle, .position length, char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCURLINE, cast(core.sys.windows.windef.WPARAM)(length), cast(core.sys.windows.windef.LPARAM)(text)));
	}

.position send_SCI_GETENDSTYLED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETENDSTYLED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_CONVERTEOLS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CONVERTEOLS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETEOLMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETEOLMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETEOLMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETEOLMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STARTSTYLING(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STARTSTYLING, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETSTYLING(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSTYLING, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETBUFFEREDDRAW(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETBUFFEREDDRAW, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETBUFFEREDDRAW(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETBUFFEREDDRAW, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETTABWIDTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETTABWIDTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETTABWIDTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTABWIDTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_CLEARTABSTOPS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CLEARTABSTOPS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_ADDTABSTOP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ADDTABSTOP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETNEXTTABSTOP(core.sys.windows.windef.HWND _scintillaHandle, .line line, core.stdc.stdint.intptr_t x)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETNEXTTABSTOP, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(x)));
	}

/+
void send_SCI_SETCODEPAGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCODEPAGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETIMEINTERACTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETIMEINTERACTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETIMEINTERACTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETIMEINTERACTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARKERDEFINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERDEFINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARKERSETFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERSETFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARKERSETBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERSETBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARKERSETBACKSELECTED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERSETBACKSELECTED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARKERENABLEHIGHLIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERENABLEHIGHLIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_MARKERADD(core.sys.windows.windef.HWND _scintillaHandle, .line line, core.stdc.stdint.intptr_t markerNumber)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERADD, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(markerNumber)));
	}

/+
void send_SCI_MARKERDELETE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERDELETE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARKERDELETEALL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERDELETEALL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_MARKERGET(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERGET, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.line send_SCI_MARKERNEXT(core.sys.windows.windef.HWND _scintillaHandle, .line lineStart, core.stdc.stdint.intptr_t markerMask)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERNEXT, cast(core.sys.windows.windef.WPARAM)(lineStart), cast(core.sys.windows.windef.LPARAM)(markerMask)));
	}

.line send_SCI_MARKERPREVIOUS(core.sys.windows.windef.HWND _scintillaHandle, .line lineStart, core.stdc.stdint.intptr_t markerMask)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERPREVIOUS, cast(core.sys.windows.windef.WPARAM)(lineStart), cast(core.sys.windows.windef.LPARAM)(markerMask)));
	}

/+
void send_SCI_MARKERDEFINEPIXMAP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERDEFINEPIXMAP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARKERADDSET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERADDSET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARKERSETALPHA(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERSETALPHA, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETMARGINTYPEN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINTYPEN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMARGINTYPEN(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t margin)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINTYPEN, cast(core.sys.windows.windef.WPARAM)(margin), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMARGINWIDTHN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINWIDTHN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMARGINWIDTHN(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t margin)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINWIDTHN, cast(core.sys.windows.windef.WPARAM)(margin), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMARGINMASKN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINMASKN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMARGINMASKN(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t margin)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINMASKN, cast(core.sys.windows.windef.WPARAM)(margin), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMARGINSENSITIVEN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINSENSITIVEN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETMARGINSENSITIVEN(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t margin)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINSENSITIVEN, cast(core.sys.windows.windef.WPARAM)(margin), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMARGINCURSORN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINCURSORN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMARGINCURSORN(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t margin)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINCURSORN, cast(core.sys.windows.windef.WPARAM)(margin), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMARGINBACKN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINBACKN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.colour send_SCI_GETMARGINBACKN(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t margin)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINBACKN, cast(core.sys.windows.windef.WPARAM)(margin), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMARGINS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMARGINS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_STYLECLEARALL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLECLEARALL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETBOLD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETBOLD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETITALIC(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETITALIC, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETSIZE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETSIZE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETFONT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETFONT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETEOLFILLED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETEOLFILLED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLERESETDEFAULT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLERESETDEFAULT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETUNDERLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETUNDERLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.colour send_SCI_STYLEGETFORE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETFORE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.colour send_SCI_STYLEGETBACK(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETBACK, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_STYLEGETBOLD(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETBOLD, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_STYLEGETITALIC(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETITALIC, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_STYLEGETSIZE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETSIZE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_STYLEGETFONT(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style, char* tags)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETFONT, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(tags)));
	}

bool send_SCI_STYLEGETEOLFILLED(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETEOLFILLED, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_STYLEGETUNDERLINE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETUNDERLINE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_STYLEGETCASE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETCASE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_STYLEGETCHARACTERSET(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETCHARACTERSET, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_STYLEGETVISIBLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETVISIBLE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_STYLEGETCHANGEABLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETCHANGEABLE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_STYLEGETHOTSPOT(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETHOTSPOT, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_STYLESETCASE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETCASE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETSIZEFRACTIONAL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETSIZEFRACTIONAL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_STYLEGETSIZEFRACTIONAL(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETSIZEFRACTIONAL, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_STYLESETWEIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETWEIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_STYLEGETWEIGHT(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLEGETWEIGHT, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_STYLESETCHARACTERSET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETCHARACTERSET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETHOTSPOT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETHOTSPOT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETSELFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETSELBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETSELALPHA(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELALPHA, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSELALPHA(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELALPHA, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETSELEOLFILLED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELEOLFILLED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSELEOLFILLED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELEOLFILLED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETCARETFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_ASSIGNCMDKEY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ASSIGNCMDKEY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CLEARCMDKEY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CLEARCMDKEY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CLEARALLCMDKEYS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CLEARALLCMDKEYS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETSTYLINGEX(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSTYLINGEX, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETVISIBLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETVISIBLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETCARETPERIOD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETPERIOD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCARETPERIOD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETPERIOD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETWORDCHARS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWORDCHARS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETWORDCHARS(core.sys.windows.windef.HWND _scintillaHandle, char* characters)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETWORDCHARS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(characters)));
	}

void send_SCI_BEGINUNDOACTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_BEGINUNDOACTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

void send_SCI_ENDUNDOACTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ENDUNDOACTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

/+
void send_SCI_INDICSETSTYLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICSETSTYLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_INDICGETSTYLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICGETSTYLE, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_INDICSETFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICSETFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.colour send_SCI_INDICGETFORE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICGETFORE, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_INDICSETUNDER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICSETUNDER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_INDICGETUNDER(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICGETUNDER, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_INDICSETHOVERSTYLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICSETHOVERSTYLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_INDICGETHOVERSTYLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICGETHOVERSTYLE, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_INDICSETHOVERFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICSETHOVERFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.colour send_SCI_INDICGETHOVERFORE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICGETHOVERFORE, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_INDICSETFLAGS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICSETFLAGS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_INDICGETFLAGS(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICGETFLAGS, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETWHITESPACEFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWHITESPACEFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETWHITESPACEBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWHITESPACEBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETWHITESPACESIZE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWHITESPACESIZE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETWHITESPACESIZE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETWHITESPACESIZE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETLINESTATE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETLINESTATE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETLINESTATE(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINESTATE, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETMAXLINESTATE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMAXLINESTATE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_GETCARETLINEVISIBLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETLINEVISIBLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCARETLINEVISIBLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETLINEVISIBLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.colour send_SCI_GETCARETLINEBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETLINEBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCARETLINEBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETLINEBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETCARETLINEFRAME(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETLINEFRAME, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCARETLINEFRAME(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETLINEFRAME, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STYLESETCHANGEABLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STYLESETCHANGEABLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_AUTOCSHOW(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSHOW, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_AUTOCCANCEL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCCANCEL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_AUTOCACTIVE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCACTIVE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_AUTOCPOSSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCPOSSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_AUTOCCOMPLETE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCCOMPLETE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_AUTOCSTOPS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSTOPS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_AUTOCSETSEPARATOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETSEPARATOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_AUTOCGETSEPARATOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETSEPARATOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_AUTOCSELECT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSELECT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_AUTOCSETCANCELATSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETCANCELATSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_AUTOCGETCANCELATSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETCANCELATSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_AUTOCSETFILLUPS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETFILLUPS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_AUTOCSETCHOOSESINGLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETCHOOSESINGLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_AUTOCGETCHOOSESINGLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETCHOOSESINGLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_AUTOCSETIGNORECASE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETIGNORECASE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_AUTOCGETIGNORECASE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETIGNORECASE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_USERLISTSHOW(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_USERLISTSHOW, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_AUTOCSETAUTOHIDE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETAUTOHIDE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_AUTOCGETAUTOHIDE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETAUTOHIDE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_AUTOCSETDROPRESTOFWORD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETDROPRESTOFWORD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_AUTOCGETDROPRESTOFWORD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETDROPRESTOFWORD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_REGISTERIMAGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_REGISTERIMAGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CLEARREGISTEREDIMAGES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CLEARREGISTEREDIMAGES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_AUTOCGETTYPESEPARATOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETTYPESEPARATOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_AUTOCSETTYPESEPARATOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETTYPESEPARATOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_AUTOCSETMAXWIDTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETMAXWIDTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_AUTOCGETMAXWIDTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETMAXWIDTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_AUTOCSETMAXHEIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETMAXHEIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_AUTOCGETMAXHEIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETMAXHEIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETINDENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETINDENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETINDENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETINDENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETUSETABS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETUSETABS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETUSETABS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETUSETABS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETLINEINDENTATION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETLINEINDENTATION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETLINEINDENTATION(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINEINDENTATION, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_GETLINEINDENTPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINEINDENTPOSITION, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_GETCOLUMN(core.sys.windows.windef.HWND _scintillaHandle, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCOLUMN, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_COUNTCHARACTERS(core.sys.windows.windef.HWND _scintillaHandle, .position start, .position end)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_COUNTCHARACTERS, cast(core.sys.windows.windef.WPARAM)(start), cast(core.sys.windows.windef.LPARAM)(end)));
	}

.position send_SCI_COUNTCODEUNITS(core.sys.windows.windef.HWND _scintillaHandle, .position start, .position end)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_COUNTCODEUNITS, cast(core.sys.windows.windef.WPARAM)(start), cast(core.sys.windows.windef.LPARAM)(end)));
	}

/+
void send_SCI_SETHSCROLLBAR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETHSCROLLBAR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETHSCROLLBAR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETHSCROLLBAR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETINDENTATIONGUIDES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETINDENTATIONGUIDES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETINDENTATIONGUIDES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETINDENTATIONGUIDES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETHIGHLIGHTGUIDE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETHIGHLIGHTGUIDE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETHIGHLIGHTGUIDE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETHIGHLIGHTGUIDE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_GETLINEENDPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINEENDPOSITION, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETCODEPAGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCODEPAGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.colour send_SCI_GETCARETFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_GETREADONLY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETREADONLY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCURRENTPOS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCURRENTPOS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETSELECTIONSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTIONSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETSELECTIONSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSELECTIONEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTIONEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETSELECTIONEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETEMPTYSELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETEMPTYSELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETPRINTMAGNIFICATION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETPRINTMAGNIFICATION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETPRINTMAGNIFICATION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPRINTMAGNIFICATION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETPRINTCOLOURMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETPRINTCOLOURMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETPRINTCOLOURMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPRINTCOLOURMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_FINDTEXT(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t searchFlags, ref npp_api.scintilla.Scintilla.Sci_TextToFind ft)

	in
	{
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FINDTEXT, cast(core.sys.windows.windef.WPARAM)(searchFlags), cast(core.sys.windows.windef.LPARAM)(&ft)));
	}

.position send_SCI_FORMATRANGE(core.sys.windows.windef.HWND _scintillaHandle, bool draw, ref npp_api.scintilla.Scintilla.Sci_RangeToFormat fr)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FORMATRANGE, cast(core.sys.windows.windef.WPARAM)(draw), cast(core.sys.windows.windef.LPARAM)(&fr)));
	}

.line send_SCI_GETFIRSTVISIBLELINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETFIRSTVISIBLELINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_GETLINE(core.sys.windows.windef.HWND _scintillaHandle, .line line, char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINE, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(text)));
	}

.line send_SCI_GETLINECOUNT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINECOUNT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMARGINLEFT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINLEFT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMARGINLEFT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINLEFT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMARGINRIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINRIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMARGINRIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINRIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_GETMODIFY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMODIFY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

void send_SCI_SETSEL(core.sys.windows.windef.HWND _scintillaHandle, .position anchor, .position caret)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSEL, cast(core.sys.windows.windef.WPARAM)(anchor), cast(core.sys.windows.windef.LPARAM)(caret));
	}

.position send_SCI_GETSELTEXT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_GETSELTEXT(core.sys.windows.windef.HWND _scintillaHandle, char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(text)));
	}

.position send_SCI_GETTEXTRANGE(core.sys.windows.windef.HWND _scintillaHandle, ref npp_api.scintilla.Scintilla.Sci_TextRange tr)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTEXTRANGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(&tr)));
	}

/+
void send_SCI_HIDESELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_HIDESELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_POINTXFROMPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_POINTXFROMPOSITION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(pos)));
	}

core.stdc.stdint.intptr_t send_SCI_POINTYFROMPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_POINTYFROMPOSITION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(pos)));
	}

.line send_SCI_LINEFROMPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEFROMPOSITION, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_POSITIONFROMLINE(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_POSITIONFROMLINE, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_LINESCROLL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINESCROLL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SCROLLCARET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SCROLLCARET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SCROLLRANGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SCROLLRANGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

void send_SCI_REPLACESEL(core.sys.windows.windef.HWND _scintillaHandle, const char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_REPLACESEL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(text));
	}

/+
void send_SCI_SETREADONLY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETREADONLY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_NULL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_NULL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_CANPASTE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CANPASTE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_CANUNDO(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CANUNDO, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_EMPTYUNDOBUFFER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_EMPTYUNDOBUFFER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

void send_SCI_UNDO(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_UNDO, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

void send_SCI_CUT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CUT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

void send_SCI_COPY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_COPY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

void send_SCI_PASTE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PASTE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

void send_SCI_CLEAR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CLEAR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

void send_SCI_SETTEXT(core.sys.windows.windef.HWND _scintillaHandle, const char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(text));
	}

.position send_SCI_GETTEXT(core.sys.windows.windef.HWND _scintillaHandle, .position length, char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTEXT, cast(core.sys.windows.windef.WPARAM)(length), cast(core.sys.windows.windef.LPARAM)(text)));
	}

.position send_SCI_GETTEXTLENGTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTEXTLENGTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.pointer send_SCI_GETDIRECTFUNCTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.pointer)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETDIRECTFUNCTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.pointer send_SCI_GETDIRECTPOINTER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.pointer)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETDIRECTPOINTER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETOVERTYPE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETOVERTYPE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETOVERTYPE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETOVERTYPE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCARETWIDTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETWIDTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETCARETWIDTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETWIDTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

void send_SCI_SETTARGETSTART(core.sys.windows.windef.HWND _scintillaHandle, .position start)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETTARGETSTART, cast(core.sys.windows.windef.WPARAM)(start), cast(core.sys.windows.windef.LPARAM)(0));
	}

.position send_SCI_GETTARGETSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTARGETSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

void send_SCI_SETTARGETEND(core.sys.windows.windef.HWND _scintillaHandle, .position end)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETTARGETEND, cast(core.sys.windows.windef.WPARAM)(end), cast(core.sys.windows.windef.LPARAM)(0));
	}

.position send_SCI_GETTARGETEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTARGETEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETTARGETRANGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETTARGETRANGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETTARGETTEXT(core.sys.windows.windef.HWND _scintillaHandle, char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTARGETTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(text)));
	}

void send_SCI_TARGETFROMSELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TARGETFROMSELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}

/+
void send_SCI_TARGETWHOLEDOCUMENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TARGETWHOLEDOCUMENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_REPLACETARGET(core.sys.windows.windef.HWND _scintillaHandle, .position length, const (char)* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_REPLACETARGET, cast(core.sys.windows.windef.WPARAM)(length), cast(core.sys.windows.windef.LPARAM)(text)));
	}

.position send_SCI_REPLACETARGETRE(core.sys.windows.windef.HWND _scintillaHandle, .position length, const (char)* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_REPLACETARGETRE, cast(core.sys.windows.windef.WPARAM)(length), cast(core.sys.windows.windef.LPARAM)(text)));
	}

.position send_SCI_SEARCHINTARGET(core.sys.windows.windef.HWND _scintillaHandle, .position length, const (char)* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SEARCHINTARGET, cast(core.sys.windows.windef.WPARAM)(length), cast(core.sys.windows.windef.LPARAM)(text)));
	}

/+
void send_SCI_SETSEARCHFLAGS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSEARCHFLAGS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETSEARCHFLAGS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSEARCHFLAGS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_CALLTIPSHOW(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPSHOW, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CALLTIPCANCEL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPCANCEL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_CALLTIPACTIVE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPACTIVE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_CALLTIPPOSSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPPOSSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_CALLTIPSETPOSSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPSETPOSSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CALLTIPSETHLT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPSETHLT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CALLTIPSETBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPSETBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CALLTIPSETFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPSETFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CALLTIPSETFOREHLT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPSETFOREHLT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CALLTIPUSESTYLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPUSESTYLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CALLTIPSETPOSITION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CALLTIPSETPOSITION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.line send_SCI_VISIBLEFROMDOCLINE(core.sys.windows.windef.HWND _scintillaHandle, .line docLine)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_VISIBLEFROMDOCLINE, cast(core.sys.windows.windef.WPARAM)(docLine), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.line send_SCI_DOCLINEFROMVISIBLE(core.sys.windows.windef.HWND _scintillaHandle, .line displayLine)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DOCLINEFROMVISIBLE, cast(core.sys.windows.windef.WPARAM)(displayLine), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.line send_SCI_WRAPCOUNT(core.sys.windows.windef.HWND _scintillaHandle, .line docLine)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WRAPCOUNT, cast(core.sys.windows.windef.WPARAM)(docLine), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETFOLDLEVEL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETFOLDLEVEL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETFOLDLEVEL(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETFOLDLEVEL, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.line send_SCI_GETLASTCHILD(core.sys.windows.windef.HWND _scintillaHandle, .line line, core.stdc.stdint.intptr_t level)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLASTCHILD, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(level)));
	}

.line send_SCI_GETFOLDPARENT(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETFOLDPARENT, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SHOWLINES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SHOWLINES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_HIDELINES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_HIDELINES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETLINEVISIBLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINEVISIBLE, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_GETALLLINESVISIBLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETALLLINESVISIBLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETFOLDEXPANDED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETFOLDEXPANDED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETFOLDEXPANDED(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETFOLDEXPANDED, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_TOGGLEFOLD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TOGGLEFOLD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_TOGGLEFOLDSHOWTEXT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TOGGLEFOLDSHOWTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_FOLDDISPLAYTEXTSETSTYLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FOLDDISPLAYTEXTSETSTYLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_FOLDLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FOLDLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_FOLDCHILDREN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FOLDCHILDREN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_EXPANDCHILDREN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_EXPANDCHILDREN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_FOLDALL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FOLDALL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

void send_SCI_ENSUREVISIBLE(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ENSUREVISIBLE, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0));
	}

/+
void send_SCI_SETAUTOMATICFOLD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETAUTOMATICFOLD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETAUTOMATICFOLD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETAUTOMATICFOLD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETFOLDFLAGS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETFOLDFLAGS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_ENSUREVISIBLEENFORCEPOLICY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ENSUREVISIBLEENFORCEPOLICY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETTABINDENTS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETTABINDENTS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETTABINDENTS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTABINDENTS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETBACKSPACEUNINDENTS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETBACKSPACEUNINDENTS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETBACKSPACEUNINDENTS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETBACKSPACEUNINDENTS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMOUSEDWELLTIME(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMOUSEDWELLTIME, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMOUSEDWELLTIME(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMOUSEDWELLTIME, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_WORDSTARTPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .position pos, bool onlyWordCharacters)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDSTARTPOSITION, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(onlyWordCharacters)));
	}

.position send_SCI_WORDENDPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .position pos, bool onlyWordCharacters)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDENDPOSITION, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(onlyWordCharacters)));
	}

bool send_SCI_ISRANGEWORD(core.sys.windows.windef.HWND _scintillaHandle, .position start, .position end)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ISRANGEWORD, cast(core.sys.windows.windef.WPARAM)(start), cast(core.sys.windows.windef.LPARAM)(end)));
	}

/+
void send_SCI_SETIDLESTYLING(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETIDLESTYLING, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETIDLESTYLING(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETIDLESTYLING, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETWRAPMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWRAPMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETWRAPMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETWRAPMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETWRAPVISUALFLAGS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWRAPVISUALFLAGS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETWRAPVISUALFLAGS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETWRAPVISUALFLAGS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETWRAPVISUALFLAGSLOCATION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWRAPVISUALFLAGSLOCATION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETWRAPVISUALFLAGSLOCATION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETWRAPVISUALFLAGSLOCATION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETWRAPSTARTINDENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWRAPSTARTINDENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETWRAPSTARTINDENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETWRAPSTARTINDENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETWRAPINDENTMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWRAPINDENTMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETWRAPINDENTMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETWRAPINDENTMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETLAYOUTCACHE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETLAYOUTCACHE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETLAYOUTCACHE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLAYOUTCACHE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSCROLLWIDTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSCROLLWIDTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETSCROLLWIDTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSCROLLWIDTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSCROLLWIDTHTRACKING(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSCROLLWIDTHTRACKING, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETSCROLLWIDTHTRACKING(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSCROLLWIDTHTRACKING, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_TEXTWIDTH(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style, const (char)* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TEXTWIDTH, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(text)));
	}

/+
void send_SCI_SETENDATLASTLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETENDATLASTLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETENDATLASTLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETENDATLASTLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_TEXTHEIGHT(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TEXTHEIGHT, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETVSCROLLBAR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETVSCROLLBAR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETVSCROLLBAR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETVSCROLLBAR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

void send_SCI_APPENDTEXT(core.sys.windows.windef.HWND _scintillaHandle, .position length, const char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_APPENDTEXT, cast(core.sys.windows.windef.WPARAM)(length), cast(core.sys.windows.windef.LPARAM)(text));
	}

core.stdc.stdint.intptr_t send_SCI_GETPHASESDRAW(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPHASESDRAW, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETPHASESDRAW(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETPHASESDRAW, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETFONTQUALITY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETFONTQUALITY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETFONTQUALITY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETFONTQUALITY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETFIRSTVISIBLELINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETFIRSTVISIBLELINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETMULTIPASTE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMULTIPASTE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMULTIPASTE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMULTIPASTE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETTAG(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t tagNumber, char* tagValue)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTAG, cast(core.sys.windows.windef.WPARAM)(tagNumber), cast(core.sys.windows.windef.LPARAM)(tagValue)));
	}

/+
void send_SCI_LINESJOIN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINESJOIN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINESSPLIT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINESSPLIT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETFOLDMARGINCOLOUR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETFOLDMARGINCOLOUR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETFOLDMARGINHICOLOUR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETFOLDMARGINHICOLOUR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETACCESSIBILITY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETACCESSIBILITY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETACCESSIBILITY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETACCESSIBILITY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_LINEDOWN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEDOWN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEDOWNEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEDOWNEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEUP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEUP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEUPEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEUPEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CHARLEFT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHARLEFT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CHARLEFTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHARLEFTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CHARRIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHARRIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CHARRIGHTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHARRIGHTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDLEFT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDLEFT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDLEFTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDLEFTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDRIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDRIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDRIGHTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDRIGHTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_HOME(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_HOME, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_HOMEEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_HOMEEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEENDEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEENDEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DOCUMENTSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DOCUMENTSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DOCUMENTSTARTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DOCUMENTSTARTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DOCUMENTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DOCUMENTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DOCUMENTENDEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DOCUMENTENDEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_PAGEUP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PAGEUP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_PAGEUPEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PAGEUPEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_PAGEDOWN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PAGEDOWN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_PAGEDOWNEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PAGEDOWNEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_EDITTOGGLEOVERTYPE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_EDITTOGGLEOVERTYPE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CANCEL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CANCEL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DELETEBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DELETEBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_TAB(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TAB, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_BACKTAB(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_BACKTAB, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_NEWLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_NEWLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_FORMFEED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FORMFEED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_VCHOME(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_VCHOME, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_VCHOMEEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_VCHOMEEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_ZOOMIN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ZOOMIN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_ZOOMOUT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ZOOMOUT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DELWORDLEFT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DELWORDLEFT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DELWORDRIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DELWORDRIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DELWORDRIGHTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DELWORDRIGHTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINECUT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINECUT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEDELETE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEDELETE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINETRANSPOSE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINETRANSPOSE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEREVERSE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEREVERSE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEDUPLICATE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEDUPLICATE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LOWERCASE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LOWERCASE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_UPPERCASE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_UPPERCASE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINESCROLLDOWN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINESCROLLDOWN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINESCROLLUP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINESCROLLUP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DELETEBACKNOTLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DELETEBACKNOTLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_HOMEDISPLAY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_HOMEDISPLAY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_HOMEDISPLAYEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_HOMEDISPLAYEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEENDDISPLAY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEENDDISPLAY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEENDDISPLAYEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEENDDISPLAYEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_HOMEWRAP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_HOMEWRAP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_HOMEWRAPEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_HOMEWRAPEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEENDWRAP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEENDWRAP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEENDWRAPEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEENDWRAPEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_VCHOMEWRAP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_VCHOMEWRAP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_VCHOMEWRAPEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_VCHOMEWRAPEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINECOPY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINECOPY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MOVECARETINSIDEVIEW(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MOVECARETINSIDEVIEW, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_LINELENGTH(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINELENGTH, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_BRACEHIGHLIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_BRACEHIGHLIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_BRACEHIGHLIGHTINDICATOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_BRACEHIGHLIGHTINDICATOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_BRACEBADLIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_BRACEBADLIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_BRACEBADLIGHTINDICATOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_BRACEBADLIGHTINDICATOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_BRACEMATCH(core.sys.windows.windef.HWND _scintillaHandle, .position pos, core.stdc.stdint.intptr_t maxReStyle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_BRACEMATCH, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(maxReStyle)));
	}

bool send_SCI_GETVIEWEOL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETVIEWEOL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETVIEWEOL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETVIEWEOL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.pointer send_SCI_GETDOCPOINTER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.pointer)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETDOCPOINTER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETDOCPOINTER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETDOCPOINTER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETMODEVENTMASK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMODEVENTMASK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETEDGECOLUMN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETEDGECOLUMN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETEDGECOLUMN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETEDGECOLUMN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETEDGEMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETEDGEMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETEDGEMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETEDGEMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.colour send_SCI_GETEDGECOLOUR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETEDGECOLOUR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETEDGECOLOUR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETEDGECOLOUR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MULTIEDGEADDLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MULTIEDGEADDLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MULTIEDGECLEARALL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MULTIEDGECLEARALL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SEARCHANCHOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SEARCHANCHOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_SEARCHNEXT(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t searchFlags, const (char)* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SEARCHNEXT, cast(core.sys.windows.windef.WPARAM)(searchFlags), cast(core.sys.windows.windef.LPARAM)(text)));
	}

.position send_SCI_SEARCHPREV(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t searchFlags, const (char)* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SEARCHPREV, cast(core.sys.windows.windef.WPARAM)(searchFlags), cast(core.sys.windows.windef.LPARAM)(text)));
	}

.line send_SCI_LINESONSCREEN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINESONSCREEN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_USEPOPUP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_USEPOPUP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_SELECTIONISRECTANGLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SELECTIONISRECTANGLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

void send_SCI_SETZOOM(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t zoomInPoints)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETZOOM, cast(core.sys.windows.windef.WPARAM)(zoomInPoints), cast(core.sys.windows.windef.LPARAM)(0));
	}

core.stdc.stdint.intptr_t send_SCI_GETZOOM(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETZOOM, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.pointer send_SCI_CREATEDOCUMENT(core.sys.windows.windef.HWND _scintillaHandle, .position bytes, core.stdc.stdint.intptr_t documentOptions)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.pointer)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CREATEDOCUMENT, cast(core.sys.windows.windef.WPARAM)(bytes), cast(core.sys.windows.windef.LPARAM)(documentOptions)));
	}

/+
void send_SCI_ADDREFDOCUMENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ADDREFDOCUMENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_RELEASEDOCUMENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_RELEASEDOCUMENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETDOCUMENTOPTIONS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETDOCUMENTOPTIONS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETMODEVENTMASK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMODEVENTMASK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCOMMANDEVENTS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCOMMANDEVENTS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETCOMMANDEVENTS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCOMMANDEVENTS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETFOCUS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETFOCUS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETFOCUS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETFOCUS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSTATUS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSTATUS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETSTATUS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSTATUS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMOUSEDOWNCAPTURES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMOUSEDOWNCAPTURES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETMOUSEDOWNCAPTURES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMOUSEDOWNCAPTURES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMOUSEWHEELCAPTURES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMOUSEWHEELCAPTURES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETMOUSEWHEELCAPTURES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMOUSEWHEELCAPTURES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCURSOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCURSOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETCURSOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCURSOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCONTROLCHARSYMBOL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCONTROLCHARSYMBOL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETCONTROLCHARSYMBOL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCONTROLCHARSYMBOL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_WORDPARTLEFT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDPARTLEFT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDPARTLEFTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDPARTLEFTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDPARTRIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDPARTRIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDPARTRIGHTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDPARTRIGHTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETVISIBLEPOLICY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETVISIBLEPOLICY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DELLINELEFT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DELLINELEFT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DELLINERIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DELLINERIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETXOFFSET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETXOFFSET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETXOFFSET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETXOFFSET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_CHOOSECARETX(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHOOSECARETX, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_GRABFOCUS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GRABFOCUS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETXCARETPOLICY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETXCARETPOLICY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETYCARETPOLICY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETYCARETPOLICY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETPRINTWRAPMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETPRINTWRAPMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETPRINTWRAPMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPRINTWRAPMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETHOTSPOTACTIVEFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETHOTSPOTACTIVEFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.colour send_SCI_GETHOTSPOTACTIVEFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETHOTSPOTACTIVEFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETHOTSPOTACTIVEBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETHOTSPOTACTIVEBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.colour send_SCI_GETHOTSPOTACTIVEBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETHOTSPOTACTIVEBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETHOTSPOTACTIVEUNDERLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETHOTSPOTACTIVEUNDERLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETHOTSPOTACTIVEUNDERLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETHOTSPOTACTIVEUNDERLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETHOTSPOTSINGLELINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETHOTSPOTSINGLELINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETHOTSPOTSINGLELINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETHOTSPOTSINGLELINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_PARADOWN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PARADOWN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_PARADOWNEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PARADOWNEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_PARAUP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PARAUP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_PARAUPEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PARAUPEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_POSITIONBEFORE(core.sys.windows.windef.HWND _scintillaHandle, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_POSITIONBEFORE, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_POSITIONAFTER(core.sys.windows.windef.HWND _scintillaHandle, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_POSITIONAFTER, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_POSITIONRELATIVE(core.sys.windows.windef.HWND _scintillaHandle, .position pos, .position relative)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_POSITIONRELATIVE, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(relative)));
	}

.position send_SCI_POSITIONRELATIVECODEUNITS(core.sys.windows.windef.HWND _scintillaHandle, .position pos, .position relative)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_POSITIONRELATIVECODEUNITS, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(relative)));
	}

/+
void send_SCI_COPYRANGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_COPYRANGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_COPYTEXT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_COPYTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETSELECTIONMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTIONMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETSELECTIONMODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONMODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_GETMOVEEXTENDSSELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMOVEEXTENDSSELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_GETLINESELSTARTPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINESELSTARTPOSITION, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.position send_SCI_GETLINESELENDPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINESELENDPOSITION, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_LINEDOWNRECTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEDOWNRECTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEUPRECTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEUPRECTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CHARLEFTRECTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHARLEFTRECTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_CHARRIGHTRECTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHARRIGHTRECTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_HOMERECTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_HOMERECTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_VCHOMERECTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_VCHOMERECTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LINEENDRECTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEENDRECTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_PAGEUPRECTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PAGEUPRECTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_PAGEDOWNRECTEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PAGEDOWNRECTEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STUTTEREDPAGEUP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STUTTEREDPAGEUP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STUTTEREDPAGEUPEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STUTTEREDPAGEUPEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STUTTEREDPAGEDOWN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STUTTEREDPAGEDOWN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STUTTEREDPAGEDOWNEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STUTTEREDPAGEDOWNEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDLEFTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDLEFTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDLEFTENDEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDLEFTENDEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDRIGHTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDRIGHTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_WORDRIGHTENDEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_WORDRIGHTENDEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETWHITESPACECHARS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETWHITESPACECHARS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETWHITESPACECHARS(core.sys.windows.windef.HWND _scintillaHandle, char* characters)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETWHITESPACECHARS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(characters)));
	}

/+
void send_SCI_SETPUNCTUATIONCHARS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETPUNCTUATIONCHARS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETPUNCTUATIONCHARS(core.sys.windows.windef.HWND _scintillaHandle, char* characters)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPUNCTUATIONCHARS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(characters)));
	}

/+
void send_SCI_SETCHARSDEFAULT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCHARSDEFAULT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_AUTOCGETCURRENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETCURRENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_AUTOCGETCURRENTTEXT(core.sys.windows.windef.HWND _scintillaHandle, char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETCURRENTTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(text)));
	}

/+
void send_SCI_AUTOCSETCASEINSENSITIVEBEHAVIOUR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETCASEINSENSITIVEBEHAVIOUR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_AUTOCGETCASEINSENSITIVEBEHAVIOUR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETCASEINSENSITIVEBEHAVIOUR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_AUTOCSETMULTI(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETMULTI, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_AUTOCGETMULTI(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETMULTI, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_AUTOCSETORDER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCSETORDER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_AUTOCGETORDER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_AUTOCGETORDER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_ALLOCATE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ALLOCATE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_TARGETASUTF8(core.sys.windows.windef.HWND _scintillaHandle, char* s)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TARGETASUTF8, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(s)));
	}

/+
void send_SCI_SETLENGTHFORENCODE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETLENGTHFORENCODE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_ENCODEDFROMUTF8(core.sys.windows.windef.HWND _scintillaHandle, const (char)* utf8, char* encoded)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ENCODEDFROMUTF8, cast(core.sys.windows.windef.WPARAM)(utf8), cast(core.sys.windows.windef.LPARAM)(encoded)));
	}

.position send_SCI_FINDCOLUMN(core.sys.windows.windef.HWND _scintillaHandle, .line line, .position column)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FINDCOLUMN, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(column)));
	}

core.stdc.stdint.intptr_t send_SCI_GETCARETSTICKY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETSTICKY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCARETSTICKY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETSTICKY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_TOGGLECARETSTICKY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TOGGLECARETSTICKY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETPASTECONVERTENDINGS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETPASTECONVERTENDINGS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETPASTECONVERTENDINGS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPASTECONVERTENDINGS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SELECTIONDUPLICATE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SELECTIONDUPLICATE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETCARETLINEBACKALPHA(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETLINEBACKALPHA, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETCARETLINEBACKALPHA(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETLINEBACKALPHA, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCARETSTYLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETSTYLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETCARETSTYLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETSTYLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETINDICATORCURRENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETINDICATORCURRENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETINDICATORCURRENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETINDICATORCURRENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETINDICATORVALUE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETINDICATORVALUE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETINDICATORVALUE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETINDICATORVALUE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_INDICATORFILLRANGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICATORFILLRANGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_INDICATORCLEARRANGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICATORCLEARRANGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_INDICATORALLONFOR(core.sys.windows.windef.HWND _scintillaHandle, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICATORALLONFOR, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_INDICATORVALUEAT(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICATORVALUEAT, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(pos)));
	}

core.stdc.stdint.intptr_t send_SCI_INDICATORSTART(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICATORSTART, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(pos)));
	}

core.stdc.stdint.intptr_t send_SCI_INDICATOREND(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator, .position pos)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICATOREND, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(pos)));
	}

/+
void send_SCI_SETPOSITIONCACHE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETPOSITIONCACHE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETPOSITIONCACHE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPOSITIONCACHE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_COPYALLOWLINE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_COPYALLOWLINE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.pointer send_SCI_GETCHARACTERPOINTER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.pointer)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCHARACTERPOINTER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.pointer send_SCI_GETRANGEPOINTER(core.sys.windows.windef.HWND _scintillaHandle, .position start, .position lengthRange)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.pointer)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETRANGEPOINTER, cast(core.sys.windows.windef.WPARAM)(start), cast(core.sys.windows.windef.LPARAM)(lengthRange)));
	}

.position send_SCI_GETGAPPOSITION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETGAPPOSITION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_INDICSETALPHA(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICSETALPHA, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_INDICGETALPHA(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICGETALPHA, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_INDICSETOUTLINEALPHA(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICSETOUTLINEALPHA, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_INDICGETOUTLINEALPHA(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t indicator)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDICGETOUTLINEALPHA, cast(core.sys.windows.windef.WPARAM)(indicator), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETEXTRAASCENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETEXTRAASCENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETEXTRAASCENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETEXTRAASCENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETEXTRADESCENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETEXTRADESCENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETEXTRADESCENT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETEXTRADESCENT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_MARKERSYMBOLDEFINED(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t markerNumber)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERSYMBOLDEFINED, cast(core.sys.windows.windef.WPARAM)(markerNumber), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_MARGINSETTEXT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARGINSETTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_MARGINGETTEXT(core.sys.windows.windef.HWND _scintillaHandle, .line line, char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARGINGETTEXT, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(text)));
	}

/+
void send_SCI_MARGINSETSTYLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARGINSETSTYLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_MARGINGETSTYLE(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARGINGETSTYLE, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_MARGINSETSTYLES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARGINSETSTYLES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_MARGINGETSTYLES(core.sys.windows.windef.HWND _scintillaHandle, .line line, char* styles)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARGINGETSTYLES, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(styles)));
	}

/+
void send_SCI_MARGINTEXTCLEARALL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARGINTEXTCLEARALL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARGINSETSTYLEOFFSET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARGINSETSTYLEOFFSET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_MARGINGETSTYLEOFFSET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARGINGETSTYLEOFFSET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMARGINOPTIONS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMARGINOPTIONS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMARGINOPTIONS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMARGINOPTIONS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_ANNOTATIONSETTEXT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONSETTEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_ANNOTATIONGETTEXT(core.sys.windows.windef.HWND _scintillaHandle, .line line, char* text)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONGETTEXT, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(text)));
	}

/+
void send_SCI_ANNOTATIONSETSTYLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONSETSTYLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_ANNOTATIONGETSTYLE(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONGETSTYLE, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_ANNOTATIONSETSTYLES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONSETSTYLES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_ANNOTATIONGETSTYLES(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t line, char* styles)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONGETSTYLES, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(styles)));
	}

core.stdc.stdint.intptr_t send_SCI_ANNOTATIONGETLINES(core.sys.windows.windef.HWND _scintillaHandle, .line line)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONGETLINES, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_ANNOTATIONCLEARALL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONCLEARALL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_ANNOTATIONSETVISIBLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONSETVISIBLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_ANNOTATIONGETVISIBLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONGETVISIBLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_ANNOTATIONSETSTYLEOFFSET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONSETSTYLEOFFSET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_ANNOTATIONGETSTYLEOFFSET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ANNOTATIONGETSTYLEOFFSET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_RELEASEALLEXTENDEDSTYLES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_RELEASEALLEXTENDEDSTYLES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_ALLOCATEEXTENDEDSTYLES(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t numberStyles)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ALLOCATEEXTENDEDSTYLES, cast(core.sys.windows.windef.WPARAM)(numberStyles), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_ADDUNDOACTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ADDUNDOACTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_CHARPOSITIONFROMPOINT(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t x, core.stdc.stdint.intptr_t y)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHARPOSITIONFROMPOINT, cast(core.sys.windows.windef.WPARAM)(x), cast(core.sys.windows.windef.LPARAM)(y)));
	}

.position send_SCI_CHARPOSITIONFROMPOINTCLOSE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t x, core.stdc.stdint.intptr_t y)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHARPOSITIONFROMPOINTCLOSE, cast(core.sys.windows.windef.WPARAM)(x), cast(core.sys.windows.windef.LPARAM)(y)));
	}

/+
void send_SCI_SETMOUSESELECTIONRECTANGULARSWITCH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMOUSESELECTIONRECTANGULARSWITCH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETMOUSESELECTIONRECTANGULARSWITCH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMOUSESELECTIONRECTANGULARSWITCH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETMULTIPLESELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMULTIPLESELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETMULTIPLESELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMULTIPLESELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETADDITIONALSELECTIONTYPING(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETADDITIONALSELECTIONTYPING, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETADDITIONALSELECTIONTYPING(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETADDITIONALSELECTIONTYPING, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETADDITIONALCARETSBLINK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETADDITIONALCARETSBLINK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETADDITIONALCARETSBLINK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETADDITIONALCARETSBLINK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETADDITIONALCARETSVISIBLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETADDITIONALCARETSVISIBLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETADDITIONALCARETSVISIBLE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETADDITIONALCARETSVISIBLE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETSELECTIONS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

bool send_SCI_GETSELECTIONEMPTY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONEMPTY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_CLEARSELECTIONS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CLEARSELECTIONS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETSELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_ADDSELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ADDSELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_DROPSELECTIONN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DROPSELECTIONN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETMAINSELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETMAINSELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETMAINSELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETMAINSELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSELECTIONNCARET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTIONNCARET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETSELECTIONNCARET(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t selection)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONNCARET, cast(core.sys.windows.windef.WPARAM)(selection), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSELECTIONNANCHOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTIONNANCHOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETSELECTIONNANCHOR(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t selection)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONNANCHOR, cast(core.sys.windows.windef.WPARAM)(selection), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSELECTIONNCARETVIRTUALSPACE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTIONNCARETVIRTUALSPACE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETSELECTIONNCARETVIRTUALSPACE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t selection)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONNCARETVIRTUALSPACE, cast(core.sys.windows.windef.WPARAM)(selection), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSELECTIONNANCHORVIRTUALSPACE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTIONNANCHORVIRTUALSPACE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETSELECTIONNANCHORVIRTUALSPACE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t selection)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONNANCHORVIRTUALSPACE, cast(core.sys.windows.windef.WPARAM)(selection), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSELECTIONNSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTIONNSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETSELECTIONNSTART(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t selection)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONNSTART, cast(core.sys.windows.windef.WPARAM)(selection), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETSELECTIONNEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETSELECTIONNEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETSELECTIONNEND(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t selection)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSELECTIONNEND, cast(core.sys.windows.windef.WPARAM)(selection), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETRECTANGULARSELECTIONCARET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETRECTANGULARSELECTIONCARET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETRECTANGULARSELECTIONCARET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETRECTANGULARSELECTIONCARET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETRECTANGULARSELECTIONANCHOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETRECTANGULARSELECTIONANCHOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETRECTANGULARSELECTIONANCHOR(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETRECTANGULARSELECTIONANCHOR, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETRECTANGULARSELECTIONCARETVIRTUALSPACE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETRECTANGULARSELECTIONCARETVIRTUALSPACE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETRECTANGULARSELECTIONCARETVIRTUALSPACE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETRECTANGULARSELECTIONCARETVIRTUALSPACE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETRECTANGULARSELECTIONANCHORVIRTUALSPACE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETRECTANGULARSELECTIONANCHORVIRTUALSPACE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.position send_SCI_GETRECTANGULARSELECTIONANCHORVIRTUALSPACE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETRECTANGULARSELECTIONANCHORVIRTUALSPACE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETVIRTUALSPACEOPTIONS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETVIRTUALSPACEOPTIONS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETVIRTUALSPACEOPTIONS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETVIRTUALSPACEOPTIONS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETRECTANGULARSELECTIONMODIFIER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETRECTANGULARSELECTIONMODIFIER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETRECTANGULARSELECTIONMODIFIER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETRECTANGULARSELECTIONMODIFIER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETADDITIONALSELFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETADDITIONALSELFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETADDITIONALSELBACK(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETADDITIONALSELBACK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETADDITIONALSELALPHA(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETADDITIONALSELALPHA, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETADDITIONALSELALPHA(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETADDITIONALSELALPHA, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETADDITIONALCARETFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETADDITIONALCARETFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.colour send_SCI_GETADDITIONALCARETFORE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.colour)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETADDITIONALCARETFORE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_ROTATESELECTION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ROTATESELECTION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SWAPMAINANCHORCARET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SWAPMAINANCHORCARET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MULTIPLESELECTADDNEXT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MULTIPLESELECTADDNEXT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MULTIPLESELECTADDEACH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MULTIPLESELECTADDEACH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_CHANGELEXERSTATE(core.sys.windows.windef.HWND _scintillaHandle, .position start, .position end)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CHANGELEXERSTATE, cast(core.sys.windows.windef.WPARAM)(start), cast(core.sys.windows.windef.LPARAM)(end)));
	}

.line send_SCI_CONTRACTEDFOLDNEXT(core.sys.windows.windef.HWND _scintillaHandle, .line lineStart)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CONTRACTEDFOLDNEXT, cast(core.sys.windows.windef.WPARAM)(lineStart), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_VERTICALCENTRECARET(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_VERTICALCENTRECARET, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MOVESELECTEDLINESUP(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MOVESELECTEDLINESUP, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MOVESELECTEDLINESDOWN(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MOVESELECTEDLINESDOWN, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETIDENTIFIER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETIDENTIFIER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETIDENTIFIER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETIDENTIFIER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_RGBAIMAGESETWIDTH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_RGBAIMAGESETWIDTH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_RGBAIMAGESETHEIGHT(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_RGBAIMAGESETHEIGHT, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_RGBAIMAGESETSCALE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_RGBAIMAGESETSCALE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_MARKERDEFINERGBAIMAGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_MARKERDEFINERGBAIMAGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_REGISTERRGBAIMAGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_REGISTERRGBAIMAGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SCROLLTOSTART(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SCROLLTOSTART, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SCROLLTOEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SCROLLTOEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETTECHNOLOGY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETTECHNOLOGY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETTECHNOLOGY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETTECHNOLOGY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

.pointer send_SCI_CREATELOADER(core.sys.windows.windef.HWND _scintillaHandle, .position bytes, core.stdc.stdint.intptr_t documentOptions)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.pointer)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CREATELOADER, cast(core.sys.windows.windef.WPARAM)(bytes), cast(core.sys.windows.windef.LPARAM)(documentOptions)));
	}

/+
void send_SCI_FINDINDICATORSHOW(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FINDINDICATORSHOW, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_FINDINDICATORFLASH(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FINDINDICATORFLASH, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_FINDINDICATORHIDE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FINDINDICATORHIDE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_VCHOMEDISPLAY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_VCHOMEDISPLAY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_VCHOMEDISPLAYEXTEND(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_VCHOMEDISPLAYEXTEND, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

bool send_SCI_GETCARETLINEVISIBLEALWAYS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(bool)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETCARETLINEVISIBLEALWAYS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETCARETLINEVISIBLEALWAYS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETCARETLINEVISIBLEALWAYS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETLINEENDTYPESALLOWED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETLINEENDTYPESALLOWED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETLINEENDTYPESALLOWED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINEENDTYPESALLOWED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETLINEENDTYPESACTIVE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINEENDTYPESACTIVE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETREPRESENTATION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETREPRESENTATION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETREPRESENTATION(core.sys.windows.windef.HWND _scintillaHandle, const (char)* encodedCharacter, char* representation)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETREPRESENTATION, cast(core.sys.windows.windef.WPARAM)(encodedCharacter), cast(core.sys.windows.windef.LPARAM)(representation)));
	}

/+
void send_SCI_CLEARREPRESENTATION(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_CLEARREPRESENTATION, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STARTRECORD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STARTRECORD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_STOPRECORD(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_STOPRECORD, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETLEXER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETLEXER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETLEXER(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLEXER, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_COLOURISE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_COLOURISE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETPROPERTY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETPROPERTY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

//KEYWORDSET_MAX
/+
void send_SCI_SETKEYWORDS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETKEYWORDS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETLEXERLANGUAGE(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETLEXERLANGUAGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_LOADLEXERLIBRARY(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LOADLEXERLIBRARY, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETPROPERTY(core.sys.windows.windef.HWND _scintillaHandle, const (char)* key, char* value)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPROPERTY, cast(core.sys.windows.windef.WPARAM)(key), cast(core.sys.windows.windef.LPARAM)(value)));
	}

core.stdc.stdint.intptr_t send_SCI_GETPROPERTYEXPANDED(core.sys.windows.windef.HWND _scintillaHandle, const (char)* key, char* value)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPROPERTYEXPANDED, cast(core.sys.windows.windef.WPARAM)(key), cast(core.sys.windows.windef.LPARAM)(value)));
	}

core.stdc.stdint.intptr_t send_SCI_GETPROPERTYINT(core.sys.windows.windef.HWND _scintillaHandle, const (char)* key, char* value)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPROPERTYINT, cast(core.sys.windows.windef.WPARAM)(key), cast(core.sys.windows.windef.LPARAM)(value)));
	}

core.stdc.stdint.intptr_t send_SCI_GETLEXERLANGUAGE(core.sys.windows.windef.HWND _scintillaHandle, char* language)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLEXERLANGUAGE, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(language)));
	}

.pointer send_SCI_PRIVATELEXERCALL(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t operation, .pointer pointer)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.pointer)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PRIVATELEXERCALL, cast(core.sys.windows.windef.WPARAM)(operation), cast(core.sys.windows.windef.LPARAM)(pointer)));
	}

core.stdc.stdint.intptr_t send_SCI_PROPERTYNAMES(core.sys.windows.windef.HWND _scintillaHandle, char* names)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PROPERTYNAMES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(names)));
	}

core.stdc.stdint.intptr_t send_SCI_PROPERTYTYPE(core.sys.windows.windef.HWND _scintillaHandle, const (char)* name)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_PROPERTYTYPE, cast(core.sys.windows.windef.WPARAM)(name), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_DESCRIBEPROPERTY(core.sys.windows.windef.HWND _scintillaHandle, const (char)* name, char* description)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DESCRIBEPROPERTY, cast(core.sys.windows.windef.WPARAM)(name), cast(core.sys.windows.windef.LPARAM)(description)));
	}

core.stdc.stdint.intptr_t send_SCI_DESCRIBEKEYWORDSETS(core.sys.windows.windef.HWND _scintillaHandle, char* description)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DESCRIBEKEYWORDSETS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(description)));
	}

core.stdc.stdint.intptr_t send_SCI_GETLINEENDTYPESSUPPORTED(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINEENDTYPESSUPPORTED, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_ALLOCATESUBSTYLES(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t styleBase, core.stdc.stdint.intptr_t numberStyles)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ALLOCATESUBSTYLES, cast(core.sys.windows.windef.WPARAM)(styleBase), cast(core.sys.windows.windef.LPARAM)(numberStyles)));
	}

core.stdc.stdint.intptr_t send_SCI_GETSUBSTYLESSTART(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t styleBase)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSUBSTYLESSTART, cast(core.sys.windows.windef.WPARAM)(styleBase), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETSUBSTYLESLENGTH(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t styleBase)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSUBSTYLESLENGTH, cast(core.sys.windows.windef.WPARAM)(styleBase), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETSTYLEFROMSUBSTYLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t subStyle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSTYLEFROMSUBSTYLE, cast(core.sys.windows.windef.WPARAM)(subStyle), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETPRIMARYSTYLEFROMSTYLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETPRIMARYSTYLEFROMSTYLE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_FREESUBSTYLES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_FREESUBSTYLES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_SETIDENTIFIERS(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETIDENTIFIERS, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_DISTANCETOSECONDARYSTYLES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DISTANCETOSECONDARYSTYLES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_GETSUBSTYLEBASES(core.sys.windows.windef.HWND _scintillaHandle, char* styles)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETSUBSTYLEBASES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(styles)));
	}

core.stdc.stdint.intptr_t send_SCI_GETNAMEDSTYLES(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETNAMEDSTYLES, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

core.stdc.stdint.intptr_t send_SCI_NAMEOFSTYLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style, char* name)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_NAMEOFSTYLE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(name)));
	}

core.stdc.stdint.intptr_t send_SCI_TAGSOFSTYLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style, char* tags)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_TAGSOFSTYLE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(tags)));
	}

core.stdc.stdint.intptr_t send_SCI_DESCRIPTIONOFSTYLE(core.sys.windows.windef.HWND _scintillaHandle, core.stdc.stdint.intptr_t style, char* description)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_DESCRIPTIONOFSTYLE, cast(core.sys.windows.windef.WPARAM)(style), cast(core.sys.windows.windef.LPARAM)(description)));
	}

//static if (!__traits(compiles, .SCI_DISABLE_PROVISIONAL)) {
core.stdc.stdint.intptr_t send_SCI_GETBIDIRECTIONAL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETBIDIRECTIONAL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_SETBIDIRECTIONAL(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_SETBIDIRECTIONAL, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

core.stdc.stdint.intptr_t send_SCI_GETLINECHARACTERINDEX(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(core.stdc.stdint.intptr_t)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_GETLINECHARACTERINDEX, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));
	}

/+
void send_SCI_ALLOCATELINECHARACTERINDEX(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_ALLOCATELINECHARACTERINDEX, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

/+
void send_SCI_RELEASELINECHARACTERINDEX(core.sys.windows.windef.HWND _scintillaHandle)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_RELEASELINECHARACTERINDEX, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0));
	}
+/

.line send_SCI_LINEFROMINDEXPOSITION(core.sys.windows.windef.HWND _scintillaHandle, .position pos, core.stdc.stdint.intptr_t lineCharacterIndex)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.line)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_LINEFROMINDEXPOSITION, cast(core.sys.windows.windef.WPARAM)(pos), cast(core.sys.windows.windef.LPARAM)(lineCharacterIndex)));
	}

.position send_SCI_INDEXPOSITIONFROMLINE(core.sys.windows.windef.HWND _scintillaHandle, .line line, core.stdc.stdint.intptr_t lineCharacterIndex)

	in
	{
		assert(_scintillaHandle != core.sys.windows.windef.NULL);
	}

	do
	{
		return cast(.position)(core.sys.windows.winuser.SendMessageA(_scintillaHandle, npp_api.scintilla.Scintilla.SCI_INDEXPOSITIONFROMLINE, cast(core.sys.windows.windef.WPARAM)(line), cast(core.sys.windows.windef.LPARAM)(lineCharacterIndex)));
	}
//}
